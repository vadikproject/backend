<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<html>
<%@include file="templates/headSection.jsp" %>
<body>
<%@include file="templates/header.jsp" %>

<div class="container">

    <div id="page-wrapper">

        <div class="row">

            <div class="col-md-3">
                <div class="list-group">
                    <a class="list-group-item active">Profile</a>
                    <a href="#" class="list-group-item">My cars</a>
                    <a href="#" class="list-group-item">My requests</a>
                </div>
            </div>

            <div class="col-md-7">
                <h1 class="page-header">Profile</h1>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-2">Username</label>
                        <div class="col-sm-4 form-output">${user.username}</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Firstname</label>
                        <div class="col-sm-4 form-output">${user.firstname}</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Surname</label>
                        <div class="col-sm-4 form-output">${user.surname}</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Middlename</label>
                        <div class="col-sm-4 form-output">${user.middlename}</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Email</label>
                        <div class="col-sm-4 form-output">${user.email}</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Skype</label>
                        <div class="col-sm-4 form-output">${user.skypeName}</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">Phone</label>
                        <div class="col-sm-4 form-output">${user.phone}</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">City</label>
                        <div class="col-sm-4 form-output">${user.city}</div>
                    </div>
                    <div class="form-group">
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <a href="<s:url value="all" />" class="btn btn-default">Back to list</a>
                        </sec:authorize>
                        <sec:authorize access="@userAccessCheck.checkEditProfileAccess(authentication, request, '${user.username}')">
                            <a href="<s:url value="${user.username}/edit" />" class="btn btn-primary">Edit</a>
                        </sec:authorize>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <%@include file="templates/footer.jsp" %>
</div>


</body>
</html>