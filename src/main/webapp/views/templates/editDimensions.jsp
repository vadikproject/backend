<div class="panel panel-default">
    <div class="panel-body form-group">
        <label class="control-label col-sm-2">Height</label>
        <div class="col-sm-4 form-output">${dimensions.height}</div>
        <label class="control-label col-sm-2">Width</label>
        <div class="col-sm-4 form-output">${dimensions.width}</div>
        <label class="control-label col-sm-2">Depth</label>
        <div class="col-sm-4 form-output">${dimensions.depth}</div>
        <label class="control-label col-sm-2">Volume</label>
        <div class="col-sm-4 form-output">${dimensions.volume}</div>
    </div>
</div>