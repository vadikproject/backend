<div class="panel panel-default">
    <div class="panel-heading">Route info</div>
    <div class="panel-body">
        <div class="form-group">
            <label class="control-label col-sm-2">Start date</label>
            <div class="col-sm-4 form-output">${cargo.startDate}</div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">End date</label>
            <div class="col-sm-4 form-output">${cargo.endDate}</div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Length</label>
            <div class="col-sm-4 form-output">${cargo.routeLength}</div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Start point</label>
            <div class="col-sm-4 form-output">${cargo.startPoint.title}</div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">End point</label>
            <div class="col-sm-4 form-output">${cargo.endPoint.title}</div>
        </div>
    </div>
</div>