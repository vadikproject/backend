﻿<!-- Navigation
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">-->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="navbar-header">

        <a class="navbar-brand" href="/">Terra</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">

        <sec:authorize access="!isAuthenticated() or isRememberMe()">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="<s:url value="login"/>"><i class="fa fa-sign-out fa-fw"></i> Login</a>
                    </li>
                    <li><a href="<s:url value="register"/>"><i class="fa fa-sign-out fa-fw"></i> Register</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </sec:authorize>

        <sec:authorize access="isFullyAuthenticated()">
            <sec:authentication var="principal" property="principal" />
            <s:url value="users/{username}" var="profileUrl">
                <s:param name="username" value="${principal.username}" />
            </s:url>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i>
                    ${principal.username}
                    <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="${profileUrl}"><i class="fa fa-user fa-fw"></i> User Profile</a>
                    </li>
                    <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </sec:authorize>
    </ul>
    <!-- /.navbar-top-links -->


</nav>


