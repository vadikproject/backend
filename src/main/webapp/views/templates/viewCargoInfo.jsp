<div class="panel panel-default">
    <div class="panel-heading">Cargo info</div>
    <div class="panel-body">
        <div class="form-group">
            <label class="control-label col-sm-2">Title</label>
            <div class="col-sm-4 form-output">${cargo.title}</div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Weight</label>
            <div class="col-sm-4 form-output">${cargo.weight}</div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Dangerous class</label>
            <div class="col-sm-4 form-output">${cargo.dangerousClass}</div>
        </div>
        <jsp:include page="/views/templates/viewDimensions.jsp" />
    </div>
</div>