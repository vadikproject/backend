<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<html>
<%@include file="templates/headSection.jsp" %>
<body>
    <%@include file="templates/header.jsp" %>

    <h1>Welcome</h1>
        <div>
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <a href="<s:url value="users/all" />">Users list</a>
            </sec:authorize>
        </div>
        <sec:authorize access="isFullyAuthenticated()">
            <div>
                <a href="<s:url value="cargo-requests/all" />">Cargo list</a>
            </div>
            <div>
                <a href="<s:url value="vehicle-requests/all" />">Vehicle list</a>
            </div>
        </sec:authorize>
    <%@include file="templates/footer.jsp" %>

</body>
</html>