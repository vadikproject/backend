<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<html>
<%@include file="templates/headSection.jsp" %>
<body>
<%@include file="templates/header.jsp" %>

<div class="container">

    <div id="page-wrapper">

        <div class="row">

            <h1 class="page-header">All users</h1>

            <div class="col-md-7">

                <c:forEach var="user" items="${users}">
                    <a href="<s:url value="${user.username}" />">
                        <div>
                            <div class="col-md-3">
                                    ${user.id}
                            </div>
                            <div class="col-md-9">
                                    ${user.username}
                            </div>
                        </div>
                    </a>
                </c:forEach>
            </div>

        </div>

    </div>
    <%@include file="templates/footer.jsp" %>
</div>


</body>
</html>