<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<html>
<%@include file="templates/headSection.jsp" %>
<body>
<%@include file="templates/header.jsp" %>


<div class="container">

    <div id="page-wrapper">

        <div class="row">

            <div class="col-md-7">
                <h1 class="page-header">Login</h1>
                <c:url value="/j_spring_security_check" var="loginUrl"/>
                <form role="form" method="POST" action="${loginUrl}">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" id="username" name="username"
                               placeholder="Enter username...">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password"
                               placeholder="Enter password...">
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" id="rememberme" name="remember-me"> Remember me</label>
                    </div>
                    <input type="hidden" name="${_csrf.parameterName}"
                           value="${_csrf.token}"/>
                    <button type="submit" class="btn btn-primary">Login</button>
                </form>
            </div>

        </div>
    </div>
    <%@include file="templates/footer.jsp" %>
</div>


</body>
</html>