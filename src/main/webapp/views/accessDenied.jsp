<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<html>
<%@include file="templates/headSection.jsp" %>
<body>
    <%@include file="templates/header.jsp" %>

    <h1>Access denied</h1>
    <div>You don&#039t deserve this, ${principal.username}!</div>

    <%@include file="templates/footer.jsp" %>

</body>
</html>