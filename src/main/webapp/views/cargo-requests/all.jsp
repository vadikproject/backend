<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<html>
<%@include file="/views/templates/headSection.jsp" %>
<body>
<%@include file="/views/templates/header.jsp" %>

<div class="container">

    <div id="page-wrapper">

        <div class="row">

            <h1 class="page-header">All Cargo Requests</h1>

            <div class="col-md-7">

                <c:forEach var="cargo" items="${cargoRequests}">
                    <a href="<s:url value="${cargo.id}" />">
                        <div>
                            <div class="col-md-3">
                                ${cargo.id}
                            </div>
                            <div class="col-md-9">
                                ${cargo.title}
                            </div>
                        </div>
                    </a>
                </c:forEach>
            </div>

        </div>

    </div>
    <%@include file="/views/templates/footer.jsp" %>
</div>


</body>
</html>