<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<html>
<%@include file="/views/templates/headSection.jsp" %>
<body>
<%@include file="/views/templates/header.jsp" %>

<div class="container">

    <div id="page-wrapper">

        <div class="row">

            <div class="col-md-offset-3 col-md-7">
                <h1 class="page-header">Cargo request</h1>
                <div class="form-horizontal">
                    <c:set value="${cargo.dimensions}" var="dimensions" scope="request"/>

                    <jsp:include page="/views/templates/viewCargoInfo.jsp" />
                    <jsp:include page="/views/templates/viewRouteInfo.jsp" />

                    <div class="panel panel-default">
                        <div class="panel-heading">General info</div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label col-sm-2">Supported Load Types</label>
                                <div class="col-sm-4 form-output">
                                    <c:forEach var="loadType" items="${cargo.supportedLoadTypes}">
                                        ${loadType.title}
                                    </c:forEach>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Supported Vehicles</label>
                                <div class="col-sm-4 form-output">
                                    <c:forEach var="vehicle" items="${cargo.supportedVehicles}">
                                        ${vehicle.title}
                                    </c:forEach>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Payment info</label>
                                <div class="col-sm-4 form-output">${cargo.paymentInfo}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Username</label>
                                <div class="col-sm-4 form-output">${cargo.username}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2">Comment</label>
                                <div class="col-sm-4 form-output">${cargo.comment}</div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <a href="<s:url value="all" />" class="btn btn-default">Back to list</a>
                        <sec:authorize access="@userAccessCheck.checkEditCargoRequestAccess(authentication, request, '${cargo.id}')">
                            <a href="<s:url value="${cargo.id}/edit" />" class="btn btn-primary">Edit</a>
                        </sec:authorize>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <%@include file="/views/templates/footer.jsp" %>
</div>


</body>
</html>