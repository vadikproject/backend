<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<html>
<%@include file="templates/headSection.jsp" %>
<body>
<%@include file="templates/header.jsp" %>

<div class="container">

    <div id="page-wrapper">

        <div class="row">

            <div class="col-md-3">
                <div class="list-group">
                    <a class="list-group-item active">Profile</a>
                    <a class="list-group-item">My cars</a>
                    <a class="list-group-item">My requests</a>
                </div>
            </div>

            <div class="col-md-7">
                <h1 class="page-header">Profile</h1>

                <sf:form role="form" method="POST" commandName="user" action="edit" class="form-horizontal">
                    <sf:input type="hidden" class="form-control" id="id" name="id" path="id"  />
                    <div class="form-group">
                        <sf:label for="username" path="username" class="control-label col-sm-2">Username</sf:label>
                        <div class="col-sm-6">
                            <sf:input type="text" class="form-control" id="username" name="username" path="username" readonly="true"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <sf:label for="firstname" path="firstname" class="control-label col-sm-2">Firstname</sf:label>
                        <div class="col-sm-6">
                            <sf:input type="text" class="form-control" id="firstname" name="firstname" path="firstname" />
                        </div>
                        <sf:errors path="firstname" cssClass="input-alert alert-danger col-sm-4" element="div"/>
                    </div>
                    <div class="form-group">
                        <sf:label for="surname" path="surname" class="control-label col-sm-2">Surname</sf:label>
                        <div class="col-sm-6">
                            <sf:input type="text" class="form-control" id="surname" name="surname" path="surname" />
                        </div>
                        <sf:errors path="surname" cssClass="input-alert alert-danger col-sm-4" element="div"/>
                    </div>
                    <div class="form-group">
                        <sf:label for="middlename" path="middlename" class="control-label col-sm-2">Middlename</sf:label>
                        <div class="col-sm-6">
                            <sf:input type="text" class="form-control" id="middlename" name="middlename" path="middlename" />
                        </div>
                        <sf:errors path="middlename" cssClass="input-alert alert-danger col-sm-4" element="div"/>
                    </div>
                    <div class="form-group">
                        <sf:label for="email" path="email" class="control-label col-sm-2">Email</sf:label>
                        <div class="col-sm-6">
                            <sf:input type="email" class="form-control" id="email" name="email" path="email" />
                        </div>
                        <sf:errors path="email" cssClass="input-alert alert-danger col-sm-4" element="div"/>
                    </div>
                    <div class="form-group">
                        <sf:label for="skypeName" path="skypeName" class="control-label col-sm-2">Skype</sf:label>
                        <div class="col-sm-6">
                            <sf:input type="text" class="form-control" id="skypeName" name="skypeName" path="skypeName" />
                        </div>
                        <sf:errors path="skypeName" cssClass="input-alert alert-danger col-sm-4" element="div"/>
                    </div>
                    <div class="form-group">
                        <sf:label for="phone" path="phone" class="control-label col-sm-2">Phone</sf:label>
                        <div class="col-sm-6">
                            <sf:input type="text" class="form-control" id="phone" name="phone" path="phone" />
                        </div>
                        <sf:errors path="phone" cssClass="input-alert alert-danger col-sm-4" element="div"/>
                    </div>
                    <div class="form-group">
                        <sf:label for="city" path="city" class="control-label col-sm-2">City</sf:label>
                        <div class="col-sm-6">
                            <sf:input type="text" class="form-control" id="city" name="city" path="city" />
                        </div>
                        <sf:errors path="city" cssClass="input-alert alert-danger col-sm-4" element="div"/>
                    </div>
                    <div class="form-group">
                        <a href="<s:url value="/users/${user.username}" />" class="btn btn-default">Back to profile</a>
                        <sf:button type="submit" class="btn btn-primary">Update</sf:button>
                    </div>
                </sf:form>
            </div>
        </div>
    </div>
    <%@include file="templates/footer.jsp" %>
</div>


</body>
</html>