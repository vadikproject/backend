<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<html>
<%@include file="../templates/headSection.jsp" %>
<body>
<%@include file="../templates/header.jsp" %>

<div class="container">

    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-10">
                <h1 class="page-header">Add new vehicle request</h1>
                <sf:form role="form" method="POST" commandName="vehicleRequest" class="form-horizontal">
                    <div class="form-group">
                        <sf:label class="control-label col-sm-2" for="startDate"
                                  path="startDate">startDate</sf:label>
                        <div class="col-sm-6">
                            <sf:input type="date" class="form-control" id="startDate"
                                      name="startDate"
                                      path="startDate"/>
                        </div>
                        <sf:errors path="startDate" cssClass="input-alert alert-danger col-sm-4" element="div"/>
                    </div>
                    <div class="form-group">
                        <sf:label class="control-label col-sm-2" for="endDate"
                                  path="endDate">endDate</sf:label>
                        <div class="col-sm-6">
                            <sf:input type="date" class="form-control" id="endDate"
                                      name="endDate"
                                      path="endDate"/>
                        </div>
                        <sf:errors path="endDate" cssClass="input-alert alert-danger col-sm-4" element="div"/>
                    </div>
                    <div class="form-group">
                        <sf:label class="control-label col-sm-2" for="routeLength"
                                  path="routeLength">routeLength</sf:label>
                        <div class="col-sm-6">
                            <sf:input type="text" class="form-control" id="routeLength"
                                      name="routeLength"
                                      path="routeLength"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <sf:label class="control-label col-sm-2" for="paymentInfo"
                                  path="paymentInfo">paymentInfo</sf:label>
                        <div class="col-sm-6">
                            <sf:input type="text" class="form-control" id="paymentInfo" name="paymentInfo"
                                      path="paymentInfo"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <sf:label class="control-label col-sm-2" for="comment" path="comment">comment</sf:label>
                        <div class="col-sm-6">
                            <sf:input type="text" class="form-control" id="comment" name="comment" path="comment"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <sf:label class="control-label col-sm-2" for="startPoint"
                                  path="startPoint">startPoint</sf:label>
                        <div class="col-sm-6">
                            <sf:select type="text" class="form-control" id="startPoint"
                                       name="startPoint" path="startPoint" items="${points}" itemValue="id"
                                       itemLabel="title"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <sf:label class="control-label col-sm-2" for="endPoint"
                                  path="endPoint">endPoint</sf:label>
                        <div class="col-sm-6">
                            <sf:select type="text" class="form-control" id="endPoint"
                                       name="endPoint" path="endPoint" items="${points}" itemValue="id"
                                       itemLabel="title"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <sf:label class="control-label col-sm-2" for="vehicleDict"
                                  path="vehicleDict">vehicleDict</sf:label>
                        <div class="col-sm-6">
                            <sf:select type="text" class="form-control" id="vehicleDict"
                                       name="vehicleDict" path="vehicleDict" items="${vehicles}"
                                       itemValue="id"
                                       itemLabel="title"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <sf:label class="control-label col-sm-2" for="vehicleNumber"
                                  path="vehicleNumber">vehicleNumber</sf:label>
                        <div class="col-sm-6">
                            <sf:input type="text" class="form-control" id="vehicleNumber"
                                      name="vehicleNumber" path="vehicleNumber"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <sf:label class="control-label col-sm-2" for="manufactureYear"
                                  path="manufactureYear">manufactureYear</sf:label>
                        <div class="col-sm-6">
                            <sf:input type="text" class="form-control" id="manufactureYear"
                                      name="manufactureYear" path="manufactureYear"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <sf:label class="control-label col-sm-2" for="height"
                                  path="height">height</sf:label>
                        <div class="col-sm-6">
                            <sf:input type="text" class="form-control" id="height"
                                      name="height" path="height"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <sf:label class="control-label col-sm-2" for="width"
                                  path="width">width</sf:label>
                        <div class="col-sm-6">
                            <sf:input type="text" class="form-control" id="width"
                                      name="width" path="width"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <sf:label class="control-label col-sm-2" for="depth"
                                  path="depth">depth</sf:label>
                        <div class="col-sm-6">
                            <sf:input type="text" class="form-control" id="depth"
                                      name="depth" path="depth"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <sf:label class="control-label col-sm-2" for="volume"
                                  path="volume">volume</sf:label>
                        <div class="col-sm-6">
                            <sf:input type="text" class="form-control" id="volume"
                                      name="volume" path="volume"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <sf:label class="control-label col-sm-2" for="carryingCapacity"
                                  path="carryingCapacity">carryingCapacity</sf:label>
                        <div class="col-sm-6">
                            <sf:input type="text" class="form-control" id="carryingCapacity"
                                      name="carryingCapacity" path="carryingCapacity"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <sf:label class="control-label col-sm-2" for="supportedLoadTypes"
                                  path="supportedLoadTypes">supportedLoadTypes</sf:label>
                        <div class="col-sm-6">
                            <sf:select class="form-control" id="supportedLoadTypes"
                                       name="supportedLoadTypes" path="supportedLoadTypes" multiple="true"
                                       items="${loadTypes}" itemValue="id" itemLabel="title">
                            </sf:select>
                        </div>
                    </div>

                    <sf:button type="submit" class="btn btn-success">Add</sf:button>
                </sf:form>
            </div>
        </div>
    </div>
    <%@include file="../templates/footer.jsp" %>
</div>


</body>
</html>
