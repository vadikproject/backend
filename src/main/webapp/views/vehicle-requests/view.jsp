<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<html>
<%@include file="../templates/headSection.jsp" %>
<body>
<%@include file="../templates/header.jsp" %>

<div class="container">

    <div id="page-wrapper">

        <div class="row">

            <div class="col-md-10">
                <h1 class="page-header">Vehicle Request</h1>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-2">startDate</label>
                        <div class="col-sm-4 form-output">${vehicleRequest.startDate}</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">endDate</label>
                        <div class="col-sm-4 form-output">${vehicleRequest.endDate}</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">routeLength</label>
                        <div class="col-sm-4 form-output">${vehicleRequest.routeLength}</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">paymentInfo</label>
                        <div class="col-sm-4 form-output">${vehicleRequest.paymentInfo}</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">comment</label>
                        <div class="col-sm-4 form-output">${vehicleRequest.comment}</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">startPoint</label>
                        <div class="col-sm-4 form-output">${vehicleRequest.startPoint.title}</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">endPoint</label>
                        <div class="col-sm-4 form-output">${vehicleRequest.endPoint.title}</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">vehicleDict</label>
                        <div class="col-sm-4 form-output">${vehicleRequest.vehicleDict.title}</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">comment</label>
                        <div class="col-sm-4 form-output">${vehicleRequest.vehicleNumber}</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">startPoint</label>
                        <div class="col-sm-4 form-output">${vehicleRequest.manufactureYear}</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">endPoint</label>
                        <div class="col-sm-4 form-output">${vehicleRequest.height}</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">vehicleDict</label>
                        <div class="col-sm-4 form-output">${vehicleRequest.width}</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">comment</label>
                        <div class="col-sm-4 form-output">${vehicleRequest.depth}</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">startPoint</label>
                        <div class="col-sm-4 form-output">${vehicleRequest.volume}</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">endPoint</label>
                        <div class="col-sm-4 form-output">${vehicleRequest.carryingCapacity}</div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2">supportedLoadTypes</label>
                        <div class="col-sm-4 form-output">
                            <c:forEach var="supportedLoadType" items="${vehicleRequest.supportedLoadTypes}">
                                ${supportedLoadType.title}
                            </c:forEach>
                        </div>
                    </div>

                    <div class="form-group">
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <a href="<s:url value="all" />" class="btn btn-default">Back to list</a>
                        </sec:authorize>
                        <sec:authorize
                                access="@userAccessCheck.checkEditVehicleRequestsAccess(authentication, request, '${vehicleRequest.id}')">
                            <a href="<s:url value="${vehicleRequest.id}/edit" />" class="btn btn-primary">Edit</a>
                        </sec:authorize>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <%@include file="../templates/footer.jsp" %>
</div>


</body>
</html>