package ru.terra.auto.config.web;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import ru.terra.auto.config.RootConfig;

/**
 * Created with IntelliJ IDEA.
 * User: zhogin
 * Date: 11/3/16
 * Time: 12:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class TerraWebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[] { RootConfig.class };
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[] { WebConfig.class };
    }

    @Override
    protected String[] getServletMappings() {
        return new String[] { "/" };
    }
}
