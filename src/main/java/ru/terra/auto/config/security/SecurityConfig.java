package ru.terra.auto.config.security;

/**
 * Created by zhogin on 11/28/2016.
 */
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

import ru.terra.auto.security.CustomAuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private CustomAuthenticationSuccessHandler successHandler;

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/", "/cargo-requests/all").permitAll()
                .antMatchers("/users/all").access("hasRole('ROLE_ADMIN')")
                .antMatchers("/users/", "/users/{username}", "/cargo-requests/{cargoId}", "/vehicle-requests/add", "/vehicle-requests/{vehicleRequestId}").access("isFullyAuthenticated()")
                .antMatchers("/users/{username}/edit").access("@userAccessCheck.checkEditProfileAccess(authentication, request, #username)")
                .antMatchers("/cargo-requests/{cargoId}/edit").access("@userAccessCheck.checkEditCargoRequestAccess(authentication, request, #cargoId)")
                .antMatchers("/vehicle-requests/{vehicleRequestId}/edit").access("@userAccessCheck.checkEditVehicleRequestsAccess(authentication, request, #vehicleRequestId)")
                .and().formLogin().loginPage("/login")
                .loginProcessingUrl("/j_spring_security_check")
                .failureUrl("/login?error")
                .usernameParameter("username").passwordParameter("password")
                .successHandler(successHandler)
                .and().exceptionHandling().accessDeniedPage("/access-denied")
                .and().csrf()
                .and().rememberMe().rememberMeParameter("remember-me").tokenRepository(persistentTokenRepository()).tokenValiditySeconds(86400);

        //HTTPS configuration. It needs to add a few extra settings. Please refer http://www.baeldung.com/spring-channel-security-https for details.
        //http.requiresChannel().anyRequest().requiresSecure();

    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl tokenRepositoryImpl = new JdbcTokenRepositoryImpl();
        tokenRepositoryImpl.setDataSource(dataSource);
        return tokenRepositoryImpl;
    }
}
