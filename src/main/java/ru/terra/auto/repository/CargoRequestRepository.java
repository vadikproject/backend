package ru.terra.auto.repository;

import org.springframework.stereotype.Repository;

import ru.terra.auto.model.CargoRequest;

/**
 * @author Evgeniy Kobtsev
 */
@Repository
public interface CargoRequestRepository extends GenericRepository<CargoRequest> {
}
