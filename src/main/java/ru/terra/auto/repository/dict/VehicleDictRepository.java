package ru.terra.auto.repository.dict;

import org.springframework.stereotype.Repository;

import ru.terra.auto.model.dict.VehicleDict;

/**
 * @author Evgeniy Kobtsev
 */
@Repository
public interface VehicleDictRepository extends DictGenericRepository<VehicleDict> {
}
