package ru.terra.auto.repository.dict;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import ru.terra.auto.model.dict.AbstractDict;

/**
 * @author Evgeniy Kobtsev
 */
@NoRepositoryBean
public interface DictGenericRepository<T extends AbstractDict> extends CrudRepository<T, Long> {

    T findByTitle(String title);

}
