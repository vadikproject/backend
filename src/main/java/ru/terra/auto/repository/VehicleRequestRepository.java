package ru.terra.auto.repository;

import org.springframework.stereotype.Repository;

import ru.terra.auto.model.VehicleRequest;

/**
 * @author Evgeniy Kobtsev
 */
@Repository
public interface VehicleRequestRepository extends GenericRepository<VehicleRequest> {
}
