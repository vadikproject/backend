package ru.terra.auto.repository;

import org.springframework.stereotype.Repository;

import ru.terra.auto.model.Vehicle;

/**
 * @author Evgeniy Kobtsev
 */
@Repository
public interface VehicleRepository extends GenericRepository<Vehicle> {
}
