package ru.terra.auto.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import ru.terra.auto.dto.RegistrationUserDto;
import ru.terra.auto.dto.UserDto;
import ru.terra.auto.model.UserRole;
import ru.terra.auto.service.UserService;

/**
 * Created by zhogin on 11/23/2016.
 */
@Service
@Slf4j
public class SecurityUserDetailsService implements UserDetailsService {

    private UserService userService;

    @Autowired
    public SecurityUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        RegistrationUserDto user = userService.getByUsernameWithPassword(username);

        return new org.springframework.security.core.userdetails.User(username, user.getPassword(), getGrantedAuthorities(user));

    }

    private List<GrantedAuthority> getGrantedAuthorities(UserDto userDto) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        List<UserRole> userRoles = userService.getUserRolesById(userDto.getId());
        log.info("found {} roles for user {}", userRoles, userDto.getUsername());
        userRoles.forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getRoleName())));
        return authorities;
    }
}
