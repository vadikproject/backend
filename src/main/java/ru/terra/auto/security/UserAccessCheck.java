package ru.terra.auto.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

import lombok.extern.slf4j.Slf4j;
import ru.terra.auto.dto.CargoRequestDto;
import ru.terra.auto.dto.VehicleRequestDto;
import ru.terra.auto.model.enums.UserRoleType;
import ru.terra.auto.service.CargoRequestService;
import ru.terra.auto.service.VehicleRequestService;

/**
 * @author Evgeniy Kobtsev
 */
@Component
@Slf4j
public class UserAccessCheck {

    @Autowired
    private CargoRequestService cargoRequestService;

    @Autowired
    private VehicleRequestService vehicleRequestService;

    public boolean checkEditProfileAccess(Authentication authentication, HttpServletRequest request, String username) {
        log.debug("check access user with {}", authentication.getName());
        return (request.isUserInRole(UserRoleType.ADMIN.getRoleName())) ||
                (request.isUserInRole(UserRoleType.USER.getRoleName()) && authentication.getName().equals(username));
    }

    public boolean checkEditCargoRequestAccess(Authentication authentication, HttpServletRequest request, Long cargoId) {
        log.debug("check access user with {}", authentication.getName());
        CargoRequestDto cargoRequestDto = cargoRequestService.findOneNonLazy(cargoId);
        return (cargoRequestDto != null) && ((request.isUserInRole(UserRoleType.ADMIN.getRoleName())) ||
                (request.isUserInRole(UserRoleType.USER.getRoleName()) && authentication.getName().equals(cargoRequestDto.getUsername())));
    }

    public boolean checkEditVehicleRequestAccess(Authentication authentication, HttpServletRequest request, Long vehicleId) {
        log.debug("check access user with {}", authentication.getName());
        VehicleRequestDto vehicleRequestDto = vehicleRequestService.findOne(vehicleId);
        return (vehicleRequestDto != null) && ((request.isUserInRole(UserRoleType.ADMIN.getRoleName())) ||
                (request.isUserInRole(UserRoleType.USER.getRoleName()) && authentication.getName().equals(vehicleRequestDto.getUsername())));
    }


}
