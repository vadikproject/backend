package ru.terra.auto.dto;


import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Created by zhogin on 11/30/2016.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class UserDto extends AbstractDto {

    @Size(min = 4, max = 20)
    private String username;

    @Size(min = 2, max = 20)
    private String firstname;

    @Size(min = 2, max = 20)
    private String surname;

    @Size(max = 20)
    private String middlename;

    @NotEmpty
    @Email
    private String email;

    @Size(max = 20)
    private String skypeName;

    @Size(max = 11)
    private String phone;

    @Size(max = 20)
    private String city;
}
