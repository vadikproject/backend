package ru.terra.auto.dto;


import javax.validation.constraints.Size;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Created by zhogin on 11/30/2016.
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class RegistrationUserDto extends UserDto {

    @Size(min = 4, max = 25)
    private String password;

}
