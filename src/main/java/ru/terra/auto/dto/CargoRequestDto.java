package ru.terra.auto.dto;

import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import ru.terra.auto.model.dict.LoadType;
import ru.terra.auto.model.dict.Point;
import ru.terra.auto.model.dict.VehicleDict;
import ru.terra.auto.model.embedded.Dimensions;

/**
 * @author Evgeniy Kobtsev
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class CargoRequestDto extends AbstractDto {

    private String title;

    private Double weight;

    private Integer dangerousClass;

    private Dimensions dimensions;

    private Date startDate;

    private Date endDate;

    private Double routeLength;

    private Point startPoint;

    private Point endPoint;

    private List<LoadType> supportedLoadTypes;

    private List<VehicleDict> supportedVehicles;

    private String paymentInfo;

    private String comment;

    private String username;

}
