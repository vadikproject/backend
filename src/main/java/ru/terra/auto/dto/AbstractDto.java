package ru.terra.auto.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by zhogin on 11/30/2016.
 */
@Data
@NoArgsConstructor
public abstract class AbstractDto {
    private Long id;
}
