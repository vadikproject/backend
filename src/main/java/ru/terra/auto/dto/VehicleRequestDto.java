package ru.terra.auto.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import ru.terra.auto.model.dict.LoadType;
import ru.terra.auto.model.dict.Point;
import ru.terra.auto.model.dict.VehicleDict;
import ru.terra.auto.model.enums.VehicleBodyType;

import java.util.Date;
import java.util.List;

/**
 * @author Evgeniy Kobtsev
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class VehicleRequestDto extends AbstractDto {

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date startDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date endDate;

    private Double routeLength;

    private Point startPoint;

    private Point endPoint;

    @NotEmpty
    private String paymentInfo;

    private String comment;

    private VehicleDict vehicleDict;

    @NotEmpty
    private String vehicleNumber;

    private Integer manufactureYear;

    private Double carryingCapacity;

    private Double height;

    private Double width;

    private Double depth;

    private Double volume;

    private VehicleBodyType bodyType;

    private List<LoadType> supportedLoadTypes;

    private String username;

}
