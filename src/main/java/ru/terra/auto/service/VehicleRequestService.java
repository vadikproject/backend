package ru.terra.auto.service;

import ru.terra.auto.dto.VehicleRequestDto;

/**
 * @author Evgeniy Kobtsev
 */
public interface VehicleRequestService extends GenericService<VehicleRequestDto> {

    void saveNewRequest(VehicleRequestDto dto);

    VehicleRequestDto findOneNonLazy(Long id);

}
