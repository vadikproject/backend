package ru.terra.auto.service;

import ru.terra.auto.dto.CargoRequestDto;

/**
 * @author Evgeniy Kobtsev
 */
public interface CargoRequestService extends GenericService<CargoRequestDto> {

    CargoRequestDto findOneNonLazy(Long id);

}
