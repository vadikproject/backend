package ru.terra.auto.service.impl;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.terra.auto.dto.CargoRequestDto;
import ru.terra.auto.model.CargoRequest;
import ru.terra.auto.repository.CargoRequestRepository;
import ru.terra.auto.service.CargoRequestService;

/**
 * @author Evgeniy Kobtsev
 */
@Service
public class CargoRequestServiceImpl extends GenericServiceImpl<CargoRequest, CargoRequestDto, CargoRequestRepository> implements CargoRequestService {

    private static final String FULL_MAP_NAME = "fullCargoRequestToDtoMap";
    private static final String PLAIN_MAP_NAME = "plainCargoRequestToDtoMap";

    @Autowired
    public CargoRequestServiceImpl(CargoRequestRepository repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
        initTypeMap();
    }

    private void initTypeMap() {
        TypeMap<CargoRequest, CargoRequestDto> fullTypeMap = modelMapper.createTypeMap(CargoRequest.class, CargoRequestDto.class, FULL_MAP_NAME);
        fullTypeMap.addMappings(new PropertyMap<CargoRequest, CargoRequestDto>() {
            @Override
            protected void configure() {
                map().setDimensions(source.getCargoInfo().getDimensions());
                map().setDangerousClass(source.getCargoInfo().getDangerousClass());
                map().setTitle(source.getCargoInfo().getTitle());
                map().setWeight(source.getCargoInfo().getWeight());
                map().setStartDate(source.getRouteInfo().getStartDate());
                map().setEndDate(source.getRouteInfo().getEndDate());
                map().setStartPoint(source.getRouteInfo().getStartPoint());
                map().setEndPoint(source.getRouteInfo().getEndPoint());
                map().setUsername(source.getUser().getUsername());
            }
        });
        TypeMap<CargoRequest, CargoRequestDto> plainTypeMap = modelMapper.createTypeMap(CargoRequest.class, CargoRequestDto.class, PLAIN_MAP_NAME);
        plainTypeMap.addMappings(new PropertyMap<CargoRequest, CargoRequestDto>() {
            @Override
            protected void configure() {
                skip(destination.getSupportedVehicles());
                skip(destination.getSupportedLoadTypes());
                map().setDimensions(source.getCargoInfo().getDimensions());
                map().setDangerousClass(source.getCargoInfo().getDangerousClass());
                map().setTitle(source.getCargoInfo().getTitle());
                map().setWeight(source.getCargoInfo().getWeight());
                map().setStartDate(source.getRouteInfo().getStartDate());
                map().setEndDate(source.getRouteInfo().getEndDate());
                map().setStartPoint(source.getRouteInfo().getStartPoint());
                map().setEndPoint(source.getRouteInfo().getEndPoint());
                map().setUsername(source.getUser().getUsername());
            }
        });
    }

    @Override
    protected CargoRequest convertToEntity(CargoRequestDto dto) {
        return modelMapper.map(dto, CargoRequest.class);
    }

    @Override
    protected CargoRequestDto convertToDTO(CargoRequest entity) {
        return modelMapper.map(entity, CargoRequestDto.class, FULL_MAP_NAME);
    }

    @Override
    protected void mapToExistingEntity(CargoRequest entity, CargoRequestDto dto) {
        modelMapper.map(dto, entity);
    }

    private CargoRequestDto plainConvertToDTO(CargoRequest entity) {
        return modelMapper.map(entity, CargoRequestDto.class, PLAIN_MAP_NAME);
    }

    @Override
    public CargoRequestDto findOneNonLazy(Long id) {
        CargoRequest cargoRequest = repository.findOne(id);
        return plainConvertToDTO(cargoRequest);
    }
}
