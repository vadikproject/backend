package ru.terra.auto.service.impl;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.terra.auto.dto.VehicleRequestDto;
import ru.terra.auto.model.User;
import ru.terra.auto.model.Vehicle;
import ru.terra.auto.model.VehicleRequest;
import ru.terra.auto.model.embedded.Dimensions;
import ru.terra.auto.model.embedded.RouteInfo;
import ru.terra.auto.model.embedded.VehicleSpecificParams;
import ru.terra.auto.repository.UserRepository;
import ru.terra.auto.repository.VehicleRepository;
import ru.terra.auto.repository.VehicleRequestRepository;
import ru.terra.auto.service.VehicleRequestService;

/**
 * @author Evgeniy Kobtsev
 */
@Service
public class VehicleRequestServiceImpl extends GenericServiceImpl<VehicleRequest, VehicleRequestDto, VehicleRequestRepository> implements VehicleRequestService{

    private static final String FULL_MAP_NAME = "fullVehicleRequestToDtoMap";
    private static final String PLAIN_MAP_NAME = "plainVehicleRequestToDtoMap";

    private UserRepository userRepository;

    private VehicleRepository vehicleRepository;

    @Autowired
    public VehicleRequestServiceImpl(VehicleRequestRepository repository, ModelMapper modelMapper, UserRepository userRepository, VehicleRepository vehicleRepository) {
        super(repository, modelMapper);
        this.userRepository = userRepository;
        this.vehicleRepository = vehicleRepository;
        initTypeMap();
    }

    private void initTypeMap() {
        TypeMap<VehicleRequest, VehicleRequestDto> fullTypeMapDto = modelMapper.createTypeMap(VehicleRequest.class, VehicleRequestDto.class, FULL_MAP_NAME);
        fullTypeMapDto.addMappings(new PropertyMap<VehicleRequest, VehicleRequestDto>() {
            @Override
            protected void configure() {
                map().setUsername(source.getUser().getUsername());

                map().setStartDate(source.getRouteInfo().getStartDate());
                map().setEndDate(source.getRouteInfo().getEndDate());
                map().setStartPoint(source.getRouteInfo().getStartPoint());
                map().setEndPoint(source.getRouteInfo().getEndPoint());
                map().setRouteLength(source.getRouteInfo().getRouteLength());

                map().setVehicleDict(source.getVehicle().getVehicleDict());
                map().setVehicleNumber(source.getVehicle().getVehicleNumber());
                map().setManufactureYear(source.getVehicle().getManufactureYear());
                map().setBodyType(source.getVehicle().getVehicleSpecificParams().getBodyType());
                map().setCarryingCapacity(source.getVehicle().getVehicleSpecificParams().getCarryingCapacity());
                map().setWidth(source.getVehicle().getVehicleSpecificParams().getDimensions().getWidth());
                map().setHeight(source.getVehicle().getVehicleSpecificParams().getDimensions().getHeight());
                map().setVolume(source.getVehicle().getVehicleSpecificParams().getDimensions().getVolume());
                map().setDepth(source.getVehicle().getVehicleSpecificParams().getDimensions().getDepth());
            }
        });

        TypeMap<VehicleRequest, VehicleRequestDto> plainTypeMapDto = modelMapper.createTypeMap(VehicleRequest.class, VehicleRequestDto.class, PLAIN_MAP_NAME);
        plainTypeMapDto.addMappings(new PropertyMap<VehicleRequest, VehicleRequestDto>() {
            @Override
            protected void configure() {
                skip(destination.getSupportedLoadTypes());

                map().setStartDate(source.getRouteInfo().getStartDate());
                map().setEndDate(source.getRouteInfo().getEndDate());
                map().setStartPoint(source.getRouteInfo().getStartPoint());
                map().setEndPoint(source.getRouteInfo().getEndPoint());
                map().setRouteLength(source.getRouteInfo().getRouteLength());
                map().setUsername(source.getUser().getUsername());

                map().setVehicleDict(source.getVehicle().getVehicleDict());
                map().setVehicleNumber(source.getVehicle().getVehicleNumber());
                map().setManufactureYear(source.getVehicle().getManufactureYear());
                map().setBodyType(source.getVehicle().getVehicleSpecificParams().getBodyType());
                map().setCarryingCapacity(source.getVehicle().getVehicleSpecificParams().getCarryingCapacity());
                map().setWidth(source.getVehicle().getVehicleSpecificParams().getDimensions().getWidth());
                map().setHeight(source.getVehicle().getVehicleSpecificParams().getDimensions().getHeight());
                map().setVolume(source.getVehicle().getVehicleSpecificParams().getDimensions().getVolume());
                map().setDepth(source.getVehicle().getVehicleSpecificParams().getDimensions().getDepth());
            }
        });
    }

    @Override
    protected VehicleRequest convertToEntity(VehicleRequestDto dto) {
        VehicleRequest vehicleRequest = new VehicleRequest();
        vehicleRequest.setComment(dto.getComment());
        vehicleRequest.setPaymentInfo(dto.getPaymentInfo());
        vehicleRequest.setSupportedLoadTypes(dto.getSupportedLoadTypes());
        RouteInfo routeInfo = new RouteInfo();
        routeInfo.setEndDate(dto.getEndDate());
        routeInfo.setStartDate(dto.getStartDate());
        routeInfo.setStartPoint(dto.getStartPoint());
        routeInfo.setEndPoint(dto.getEndPoint());
        routeInfo.setRouteLength(dto.getRouteLength());
        vehicleRequest.setRouteInfo(routeInfo);
        return vehicleRequest;
    }

    @Override
    protected VehicleRequestDto convertToDTO(VehicleRequest entity) {
        return modelMapper.map(entity, VehicleRequestDto.class, FULL_MAP_NAME);
    }

    @Override
    protected void mapToExistingEntity(VehicleRequest entity, VehicleRequestDto dto) {
        modelMapper.map(dto, entity);
    }

    private VehicleRequestDto plainConvertToDTO(VehicleRequest entity) {
        return modelMapper.map(entity, VehicleRequestDto.class, PLAIN_MAP_NAME);
    }

    @Override
    @Transactional
    public void saveNewRequest(VehicleRequestDto dto) {
        User user = userRepository.findByUsername(dto.getUsername());
        Vehicle vehicle = new Vehicle();
        vehicle.setUser(user);
        vehicle.setManufactureYear(dto.getManufactureYear());
        vehicle.setVehicleDict(dto.getVehicleDict());
        vehicle.setVehicleNumber(dto.getVehicleNumber());
        VehicleSpecificParams vehicleSpecificParams = new VehicleSpecificParams();
        vehicleSpecificParams.setBodyType(dto.getBodyType());
        vehicleSpecificParams.setCarryingCapacity(dto.getCarryingCapacity());
        Dimensions dimensions = new Dimensions();
        dimensions.setDepth(dto.getDepth());
        dimensions.setHeight(dto.getHeight());
        dimensions.setVolume(dto.getVolume());
        dimensions.setWidth(dto.getWidth());
        vehicleSpecificParams.setDimensions(dimensions);
        vehicle.setVehicleSpecificParams(vehicleSpecificParams);

        VehicleRequest vehicleRequest = convertToEntity(dto);
        vehicleRequest.setUser(user);
        vehicleRequest.setVehicle(vehicleRepository.save(vehicle));
        repository.save(vehicleRequest);
    }

    @Override
    public VehicleRequestDto findOneNonLazy(Long id) {
        VehicleRequest vehicleRequest = repository.findOne(id);
        return plainConvertToDTO(vehicleRequest);
    }
}
