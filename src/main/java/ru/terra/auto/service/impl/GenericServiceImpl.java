package ru.terra.auto.service.impl;

import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

import ru.terra.auto.dto.AbstractDto;
import ru.terra.auto.model.AbstractEntity;
import ru.terra.auto.repository.GenericRepository;
import ru.terra.auto.service.GenericService;

/**
 * Created by zhogin on 11/23/2016.
 */
abstract class GenericServiceImpl<E extends AbstractEntity, D extends AbstractDto, R extends GenericRepository<E>> implements GenericService<D> {

    ModelMapper modelMapper;

    R repository;

    GenericServiceImpl(R repository, ModelMapper modelMapper) {
        this.repository = repository;
        this.modelMapper = modelMapper;
    }

    protected abstract E convertToEntity(D dto);

    protected abstract D convertToDTO(E entity);

    protected abstract void mapToExistingEntity(E entity, D dto);

    @Override
    public void save(D dto) {
        E entity = convertToEntity(dto);
        repository.save(entity);
    }

    @Override
    public void update(D dto) {
        E existingEntity = repository.findOne(dto.getId());
        if (existingEntity == null) {
            throw new RuntimeException("can't update not existing entity");
        }
        mapToExistingEntity(existingEntity, dto);
        repository.save(existingEntity);
    }

    @Override
    public List<D> findAll() {
        Iterable<E> users = repository.findAll();
        List<D> dtoUsers = new ArrayList<>();
        users.forEach(user -> dtoUsers.add(convertToDTO(user)));

        return dtoUsers;
    }

    @Override
    public D findOne(Long id) {
        E entity = repository.findOne(id);
        return convertToDTO(entity);
    }

    @Override
    public boolean exists(Long id) {
        return repository.exists(id);
    }

    @Override
    public long count() {
        return repository.count();
    }

    @Override
    public void delete(Long id) {
        repository.delete(id);
    }

    @Override
    public void delete(D dto) {
        repository.delete(dto.getId());
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

}