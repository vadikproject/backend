package ru.terra.auto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.validation.Valid;

import ru.terra.auto.dto.VehicleRequestDto;
import ru.terra.auto.model.dict.LoadType;
import ru.terra.auto.model.dict.Point;
import ru.terra.auto.model.dict.VehicleDict;
import ru.terra.auto.repository.dict.LoadTypeRepository;
import ru.terra.auto.repository.dict.PointRepository;
import ru.terra.auto.repository.dict.VehicleDictRepository;
import ru.terra.auto.service.VehicleRequestService;

/**
 * Created by zhogin on 12/15/2016.
 */
@Controller
@RequestMapping(value = "/vehicle-requests")
@SessionAttributes({"loadTypes", "points", "vehicles"})
public class VehicleRequestController {

    private PointRepository pointRepository;

    private LoadTypeRepository loadTypeRepository;

    private VehicleDictRepository vehicleDictRepository;

    private VehicleRequestService vehicleRequestService;

    @Autowired
    public VehicleRequestController(PointRepository pointRepository, LoadTypeRepository loadTypeRepository, VehicleRequestService vehicleRequestService, VehicleDictRepository vehicleDictRepository) {
        this.pointRepository = pointRepository;
        this.loadTypeRepository = loadTypeRepository;
        this.vehicleRequestService = vehicleRequestService;
        this.vehicleDictRepository = vehicleDictRepository;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ModelAndView showAllVehicleRequests() {
        List<VehicleRequestDto> vehicleRequests = vehicleRequestService.findAll();
        return new ModelAndView("vehicle-requests/all", "vehicleRequests", vehicleRequests);
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView addVehicleRequest() {
        ModelAndView modelAndView = new ModelAndView("vehicle-requests/add");
        modelAndView.addObject("vehicleRequest", new VehicleRequestDto());
        return modelAndView;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ModelAndView addVehicleRequestProcessing(@ModelAttribute("vehicleRequest") @Valid VehicleRequestDto vehicleRequestDto,
                                                    Errors errors) {
        if (errors.hasErrors()) {
            return new ModelAndView("vehicle-requests/add");
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        vehicleRequestDto.setUsername(auth.getName());
        vehicleRequestService.saveNewRequest(vehicleRequestDto);
        return new ModelAndView("redirect:all");
    }

    @RequestMapping(value = "/{vehicleRequestId}", method = RequestMethod.GET)
    public ModelAndView showVehicleRequest(@PathVariable Long vehicleRequestId) {
        VehicleRequestDto vehicleRequestDto = vehicleRequestService.findOne(vehicleRequestId);
        return new ModelAndView("vehicle-requests/view", "vehicleRequest", vehicleRequestDto);
    }

    @RequestMapping(value = "/{vehicleRequestId}/edit", method = RequestMethod.GET)
    public ModelAndView showForEditVehicleRequest(@PathVariable Long vehicleRequestId) {
        VehicleRequestDto vehicleRequestDto = vehicleRequestService.findOne(vehicleRequestId);
        return new ModelAndView("vehicle-requests/edit", "vehicleRequest", vehicleRequestDto);
    }

    @RequestMapping(value = "/{vehicleRequestId}/edit", method = RequestMethod.POST)
    public ModelAndView updateVehicleRequest(@PathVariable Long vehicleRequestId,
                                             @ModelAttribute("vehicleRequest") @Valid VehicleRequestDto vehicleRequestDto,
                                             Errors errors) {
        if (errors.hasErrors()) {
            return new ModelAndView("vehicle-requests/edit");
        }
        vehicleRequestService.update(vehicleRequestDto);
        return new ModelAndView("redirect:" + vehicleRequestId);
    }

    @ModelAttribute("loadTypes")
    public Iterable<LoadType> initializeLoadTypes() {
        return loadTypeRepository.findAll();
    }

    @ModelAttribute("points")
    public Iterable<Point> initializePoints() {
        return pointRepository.findAll();
    }

    @ModelAttribute("vehicles")
    public Iterable<VehicleDict> initializeVehicles() {
        return vehicleDictRepository.findAll();
    }

    @InitBinder
    public final void initBinderUsuariosFormValidator(final WebDataBinder binder, final Locale locale) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", locale);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
}
