package ru.terra.auto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

import ru.terra.auto.dto.CargoRequestDto;
import ru.terra.auto.service.CargoRequestService;

/**
 * @author Evgeniy Kobtsev
 */
@Controller
@RequestMapping("/cargo-requests")
public class CargoRequestController {

    private CargoRequestService cargoRequestService;

    @Autowired
    public CargoRequestController(CargoRequestService cargoRequestService) {
        this.cargoRequestService = cargoRequestService;
    }

    @RequestMapping(value = "/{cargoRequestId}", method = RequestMethod.GET)
    public ModelAndView showUser(@PathVariable Long cargoRequestId) {
        CargoRequestDto cargoRequestDto = cargoRequestService.findOne(cargoRequestId);
        return new ModelAndView("cargo-requests/view", "cargo", cargoRequestDto);
    }

    @RequestMapping(value = "/{cargoRequestId}/edit", method = RequestMethod.GET)
    public ModelAndView showForEditUser(@PathVariable Long cargoRequestId) {
        CargoRequestDto cargoRequestDto = cargoRequestService.findOne(cargoRequestId);
        return new ModelAndView("cargo-requests/edit", "cargo", cargoRequestDto);
    }

    @RequestMapping(value = "/{cargoRequestId}/edit", method = RequestMethod.POST)
    public ModelAndView updateUser(@PathVariable Long cargoRequestId, @ModelAttribute("cargo") @Valid CargoRequestDto cargoRequestDto,
                                   Errors errors) {
        if (errors.hasErrors()) {
            return new ModelAndView("cargo-requests/edit");
        }
        cargoRequestService.update(cargoRequestDto);
        return new ModelAndView("redirect:cargo-requests/" + cargoRequestId);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public ModelAndView getAll() {
        return new ModelAndView("cargo-requests/all", "cargoRequests", cargoRequestService.findAll());
    }

}
