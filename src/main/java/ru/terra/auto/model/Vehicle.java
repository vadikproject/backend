package ru.terra.auto.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import ru.terra.auto.model.dict.VehicleDict;
import ru.terra.auto.model.embedded.VehicleSpecificParams;

/**
 * Created by zhogin on 12/6/2016.
 */
@Entity
@Table(name = "VEHICLE")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class Vehicle extends AbstractEntity {

    @ManyToOne
    @JoinColumn(name = "USER_ID", nullable = false)
    private User user;

    @Embedded
    private VehicleSpecificParams vehicleSpecificParams;

    @OneToOne
    @JoinColumn(name = "VEHICLE_DICT_ID", nullable = false)
    private VehicleDict vehicleDict;

    @Column(name = "VEHICLE_NUMBER")
    private String vehicleNumber;

    @Column(name = "MANUFACTURE_YEAR")
    private Integer manufactureYear;

}
