package ru.terra.auto.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Evgeniy Kobtsev
 */
@MappedSuperclass
@Data
@NoArgsConstructor
public abstract class AbstractEntity {

	@Id
	@Column(name = "ID")
	@GeneratedValue
	private Long id;

	@Transient
	private UUID uuid;

}
