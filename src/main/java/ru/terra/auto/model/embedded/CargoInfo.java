package ru.terra.auto.model.embedded;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Evgeniy Kobtsev
 */
@Embeddable
@Data
@NoArgsConstructor
public class CargoInfo {

	@Column(name = "TITLE")
	private String title;

	@Column(name = "WEIGHT")
	private Double weight;

	@Column(name = "D_CLASS")
	private Integer dangerousClass;

	@Embedded
	private Dimensions dimensions;

}
