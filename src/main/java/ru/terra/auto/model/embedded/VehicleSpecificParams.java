package ru.terra.auto.model.embedded;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Enumerated;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.terra.auto.model.enums.VehicleBodyType;

/**
 * @author Evgeniy Kobtsev
 */
@Embeddable
@Data
@NoArgsConstructor
public class VehicleSpecificParams {

    @Column(name = "BODY_TYPE")
    @Enumerated
    private VehicleBodyType bodyType;

    @Column(name = "C_CAPACITY")
    private Double carryingCapacity;

    @Embedded
    private Dimensions dimensions;

}
