package ru.terra.auto.model.embedded;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.terra.auto.model.dict.Point;

/**
 * @author Evgeniy Kobtsev
 */
@Embeddable
@Data
@NoArgsConstructor
public class RouteInfo {

	@Column(name = "START_DATE")
	private Date startDate;

	@Column(name = "END_DATE")
	private Date endDate;

	@Column(name = "LENGTH")
	private Double routeLength;

	@ManyToOne
	@JoinColumn(name = "START_POINT", nullable = false)
	private Point startPoint;

	@ManyToOne
	@JoinColumn(name = "END_POINT", nullable = false)
	private Point endPoint;

}
