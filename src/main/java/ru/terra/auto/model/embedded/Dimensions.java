package ru.terra.auto.model.embedded;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Evgeniy Kobtsev
 */
@Embeddable
@Data
@NoArgsConstructor
public class Dimensions {

	@Column(name = "HEIGHT")
	private Double height;

	@Column(name = "WIDTH")
	private Double width;

	@Column(name = "DEPTH")
	private Double depth;

	@Column(name = "VOLUME")
	private Double volume;

}
