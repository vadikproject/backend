package ru.terra.auto.model.dict;

import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Evgeniy Kobtsev
 */
@Entity
@Table(name = "LOAD_TYPE_DICT")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class LoadType extends AbstractDict {
}
